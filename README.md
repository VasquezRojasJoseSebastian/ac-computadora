# Von-Neumann vs Hardvard

```plantuml
@startwbs
<style>
wbsDiagram {
  Linecolor black
    Linecolor black
      Linecolor black
        Linecolor black
  node {
    Padding 12
    Margin 3
    HorizontalAlignment center
    MaximumWidth 300
  }
  :depth(0) {
      BackgroundColor black
      FontColor White
      FontSize 25
      RoundCorner 25
      LineColor Black
  }
  :depth(1) {
      BackgroundColor gray
      FontColor White
      FontSize 20
      RoundCorner 10
      LineColor Black
  }
  :depth(2) {
      BackgroundColor  gray
      FontSize 18
      FontColor White
      RoundCorner 10
      LineColor Black
  }
  :depth(3) {
      BackgroundColor gray
      FontSize 15
      FontColor White
      RoundCorner 10
      LineColor Black
  }
}
</style>
+ <b> Von Neumann vs Hardvard
++ <b> Von Neumann
+++ <b> Arquitectura
+++- <b> Los datos y las instrucciones(secuencia de control), Se almacenan en una misma memoria de lectura y escritura
++++ <b> No se pueden diferenciar entre datos e instrucciones al examinar una posición de memoria (Location)
+++- <b> Los contenidos de la memoria son direccionados por su ubicación(location), sin importar el tipo de datos contenido all
++++ <b> La ejecución ocurre en modo secuencial mediante la lectura de instrucciones consecutivas desde la memoria
+++ <b> Limitaciones
++++ <b> La limitación de la longitud de las instrucciones por el bus de datos, que hace que el microprocesador tenga que realizar varios accesos a memoria para buscar instrucciones complejas
+++- <b> La limitación de la velocidad de operación a causa del bus único para datos e instrucciones que no deja acceder simultáneamente a unos y otras, lo cual impide superponer ambos tiempos de acceso
++ <b> Hardvard
+++ <b> Arquitectura
++++ <b> Las instrucciones y los datos se almacenan en caches separadas para mejorar el rendimiento
+++- <b> Divide la cantidad de cach entre los dos, por lo que funciona mejor sólo cuando la frecuencia de lectura de instrucciones y de datos es aproximadamente la misma
++++ Esta arquitectura suele utilizarse en DSPs, o procesador de señal digital, usados habitualmente en productos para procesamiento de audio y video
+++ <b> Ventajas
++++ <b> El tamaño de las instrucciones no está relacionado con el de los datos, y por lo tanto puede ser optimizado para que cualquier instrucción ocupe una sola posición de memoria de programa, logrando así mayor velocidad y menor longitud de programa
+++- <b> El tiempo de acceso a las instrucciones puede superponerse con el de los datos, logrando una mayor velocidad en cada operación
@endwbs
```
# Super Computadoras
```plantuml
@startwbs
<style>
wbsDiagram {
  Linecolor black
    Linecolor black
      Linecolor black
        Linecolor black
  node {
    Padding 12
    Margin 3
    HorizontalAlignment center
    MaximumWidth 300
  }
  :depth(0) {
      BackgroundColor black
      FontColor White
      FontSize 25
      RoundCorner 25
      LineColor Black
  }
  :depth(1) {
      BackgroundColor gray
      FontColor White
      FontSize 20
      RoundCorner 10
      LineColor Black
  }
  :depth(2) {
      BackgroundColor  gray
      FontSize 18
      FontColor White
      RoundCorner 10
      LineColor Black
  }
  :depth(3) {
      BackgroundColor gray
      FontSize 15
      FontColor White
      RoundCorner 10
      LineColor Black
  }
}
</style>
+ <b> Supercomputadoras
++ <b> ¿Que es una supercomputadora?
+++ <b> Es un ordenador con capacidades de cálculo muy superiores a las comunes y están orientadas a fines específicos. La mayoría de los supercomputadores se componen de unidades menos potentes pero trabajando de forma conjunta con un objetivo común, aumentando tanto la potencia del conjunto como su rendimiento
++ <b> Supercomputadoras en México
+++ <b> Kan Balam
++++ <b> Este equipo cuenta con 1,368 procesadores AMD Opteron de 2.6 GHz, 3,016 GB de memoria RAM y 160 TB de almacenamiento, lo que le permite un procesamiento de 7 teraflops. Su sistema operativo es GNU/Linux y tiene como función estudiar los sismos silenciosos así como otros estudios científicos
++- <b> Aitzaloa
++++ <b> Este superordenador de la Universidad Autónoma Metropolitana y se llegó a conocer como el más potente de América Latina gracias a que contaba con 2,160 núcleos en procesadores Intel Xeon E5272 QuadCore y 100TB de almacenamiento
+++ <b> Atócatl
++++ <b> es un equipo híbrido que cuenta con que son utilizados para el estudio del universo mediante simulaciones, además de haber ayudado al Posgrado de Astrofísica
++- <b> Abacus
++++ <b> El equipo cuenta con 8,904 núcleos, además de 100 GPU K40 de Nvidia, junto con 1.2 Petabytes de almacenamiento y 40TB de memoria RAM el equipo es capaz de alcanzar los 400 Teraflops
+++ <b> Miztli
++++ <b> El equipo cuenta con 5,312 núcleos Intel E5-2670, 16 tarjetas NVIDIA m2090, RAM de 15,000 GB, almacenamiento de 750 TB y con sistema operativo Scientific Linux RedHat Enterprise
++- <b> Yoltla
++++ <b> Este superordenador tiene como sistema operativo Centos Linux y cuenta con un poder de 4,920 núcleos de procesamiento, 6,912 GB de memoria RAM, y un almacenamiento de 70TB lo cual le permite alcanzar un pico de 45 teraflops
+++ <b> Xiuhcoatl
++++ <b> Actualmente, este clúster cuenta con 11,032 GB de RAM, 60 TB de almacenamiento, 4,724 núcleos en CPU y 374,144 en GPU, lo que resulta en un procesamiento teórico de 250 teraflops
++- <b> Cuetlaxcoapan
++++ <b> El llamado Cuetlaxcoapan que cuenta con cerca de 6796 núcleos en CPU, una RAM de 2048 GB y unos 11520 núcleos CUDA gracias a la incorporación de tarjetas K40 Nvidia. Su almacenamiento es de 1.2 PB y alcanza un pico de 153408 teraflops 
@endwbs
```
